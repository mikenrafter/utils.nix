{
  inputs.git-ignore-nix.url = github:hercules-ci/gitignore.nix/master;

  # Name | Path | Builder | Dependencies
  outputs = { self, nixpkgs, flake-utils, git-ignore-nix }: with builtins; with nixpkgs.lib; rec {

  inherit head;

  # general {
  concat   = lists.foldl join {};
  join     = a: b: a // b; # equivalent to (//)
  flip     = trivial.flip;
  # }
  # flipN (N:1 -> 1:N) {
  # lipfN (1:N -> N:1) seems impossible...
  flipN = n: f: a: (if n>1 then flipN (n -1) else x: x) (flip f a);
   ## haskell versions
   # -- dynamic (invalid types)
   #flipN n f | n < 2 = f
   #          | _     = flipN (n -1) . flip f
   # -- static
   #flip2 f = flip  . flip f
   #flip3 f = flip2 . flip f
 ## static definitions
 #flip2    = f: a: flip  (flip f a);
 #flip3    = f: a: flip2 (flip f a);
  # }
  # setutils {
  /*
  Set   / Name(s)  / Function
  Value / Packages / Architecture
  Target(s)  /  Self.Packages
  */
  get       = s: n: s."${n}";
  wrap      = f: n: { "${n}" = f n; };
  mapTo     = n: v: concat (map (wrap (_: v)) n);
  default   = mapTo ["default"];

  listAttrs = f: s: concat (map (wrap f) s);
  using     = p: a:
   if attrsets.hasAttr "outputs" p
   then import p {system=a;} else p;

  list      = attrsets.mapAttrsToList (_: v: v); # point-free
  names     = attrsets.mapAttrsToList (n: _: n); # point-free
  # }

  # packaging {
 #__overlay = set: use: s: _: src: {
 # "${set}" = src."${set}".override (old: {
 #   overrides = src.lib.composeExtensions (old.overrides or (_: _: {}))
 #   (_: use s src);
 # });
 #};
  package'    = t: builds: pkgs:
   attrsets.mapAttrs (n: v: v pkgs n) (default (p: _: (get builds t) p t)
   // builds);
  package     = builds: _: pkgs: package' (head (names builds)) builds pkgs;

  overlay'    = n: sp: final: prev: listAttrs (get (get sp final.system)) n;
  overlay     = n: _: sp:
   listAttrs (t: overlay' [t] sp) n // default (overlay' n sp);

  standardize = slf: builds: importer: pkgs:
   let targets = names (builds true);
   in (flake-utils.lib.eachDefaultSystem (system:
   let npkgs = importer pkgs system;
       env = d: package' (head targets) (builds d) npkgs;
   in { packages = env false; devShells = env true; })
   // { overlays = overlay targets using slf.packages; }
   // { overlay  = slf.overlays.default; });
  # }

  # builders { 
  simple = p: b: pkgs:
    flipN 2 pkgs."${b}" (git-ignore-nix.lib.gitignoreSource p) { };
  derive = p: b: d: pkgs: n:
    with pkgs; stdenv.mkDerivation {
      name = n;
      src = p;
      nativeBuildInputs = d;
      buildPhase = b;
    };
  haskell = p: b: returnShellEnv: pkgs: n: (
    pkgs.haskellPackages.developPackage {
      inherit returnShellEnv;
      name = n;
      root = p;
      modifier = # use partial application (needs 1 more arg)
        pkgs.haskell.lib.compose.addBuildTools (with pkgs.haskellPackages; pkgs.lib.optional returnShellEnv ([
          cabal-install stack
        ] ++ b));
    });
  # }

  # input / set mapping utilities
  imap = {
    elm = flipN 2 substring 1; # point-free!
    # short for mapAttrs, but ignoring the name field
    mat = f: mapAttrs (_: a: f a);
  };

};

}
